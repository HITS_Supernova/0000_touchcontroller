"""
    UdObject
    ========

    This class has a "ud" attribute that can hold a user dictionary to store 
    data with the object. Meant to be inherited.
"""

__all__ = ('UdObject')
__version__ = '0.1'


class UdObject(object):
    def __init__(self, *args, **kwargs):
        super(UdObject, self).__init__(*args, **kwargs)
        
        # user dictionary
        if "ud" in kwargs:
            self.ud = kwargs["ud"]
        else:
            self.ud = {}


# -*- coding: utf-8 -*-

""" global configuration variables """

from __future__ import print_function, unicode_literals
from __future__ import division

import yaml
import os

# settings
ws_port = 8889

factory_url = u"ws://localhost:8889"

# reference to the main connection (WSProtocol)
conn = None

# app
app = None

# content (structure, tags and translated texts, will be read from yaml file)
content = None

# show text in the footer when connected via websocket
#footer_text_connected = "[connected]"
footer_text_connected = " "

# inactivity settings:
#
# how often to check for inactivity when interactive:
inactivity_check_interval = 1
# establish "inactive" after this many seconds:
inactivity_threshold_time = 120
# when inactive, how many seconds to wait between auto-cycling:
auto_cycle_interval = 20

# languages available
languages_available = ["en", "de"]

# current language
lang = languages_available[0]

# content:
#
# The "content" variable is a big, nested structure read from a YAML file 
# that defines both the selection_menu items and their language-dependent
# labels and descriptions.
# Should be made available at program startup by "load_content".
# It also contains the kv file name to be used.
# Use the environment variable below to handle different use cases with the same code.
if "TOUCH_CONTROLLER_YAML" in os.environ:
    content_file = os.environ["TOUCH_CONTROLLER_YAML"]
else:
    content_file = "content_earth_moon.yaml"
    #content_file = "content_milky_way.yaml"


# the "state" is stored in the widgets: selection menu, switches and language widget

# verbosity on console
# 0: normal
# 1+: only network commands
# 2+: local gui changes shown
verbose = 1

# switch variables
switch_label_color = [0, 0, 0, 1]
switch_color = [1, 1, 1, 1]
switch_disabled = False


def load_content(filename):
    """Load YAML file with the text/label/image content definition.

    At the beginning of the main program, put something as:
    cfg.content = cfg.load_content("file.yaml")
    """
    with open(filename, "r") as f:
        c = yaml.safe_load(f)
    return c


def test_content(content):
    """Basic testing of YAML content structure to avoid obvious errors."""

    what = "selection_menu"
    if verbose > 0: 
        print("Basic YAML file test:")
        print("- checking {}".format(what))
    for index, item in enumerate(content[what]):
        #print("--- item: ", item)
        assert (item["tag"]), "Tag incorrect in list index {}".format(index)
        assert (item["icon"]), "Icon incorrect for tag {}".format(item["tag"])
        assert (item["label"]), "label incorrect for tag {}".format(item["tag"])
        assert (item["description"]), "label incorrect for tag {}".format(item["tag"])
        for l in languages_available:
            assert (item["label"][l]), "{} label incorrect for tag {}".format(l, item["tag"])
            assert (item["description"][l]), "{} description incorrect for tag {}".format(l, item["tag"])
    if verbose > 0:
        print("- testing done.")
    return True


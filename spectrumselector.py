# -*- coding: utf-8 -*-

"""
    SpectrumSelector
    ================



"""

from __future__ import print_function, unicode_literals
from __future__ import division


__all__ = ('SpectrumSelector')
__version__ = '0.1'


from kivy.lang            import Builder
from kivy.uix.behaviors   import ButtonBehavior
from kivy.uix.image       import Image
from kivy.uix.widget      import Widget
from kivy.uix.boxlayout   import BoxLayout
from kivy.uix.gridlayout  import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button      import Button
from kivy.uix.label       import Label
from kivy.properties      import StringProperty, ListProperty,\
                                 ObjectProperty, NumericProperty, \
                                 BooleanProperty
from kivy.graphics        import Color, Line
from kivy.animation       import Animation
from math                 import log10, floor

import cfg


Builder.load_string('''

<WavelengthIndicator>:
    cols: 2
    padding: 30, 0, 0, 0
    
    Label:
        text: root.wavelength_label
    Label:
        text: root.wavelength_value
    Label:
        text: root.frequency_label
    Label:
        text: root.frequency_value
    Label:
        text: root.energy_label
    Label:
        text: root.energy_value


<SpectrumSelector>:
    
    cursor: cursor_id
    spectrum: spectrum_id
    
    # e-m spectrum image as background
    Spectrum:
        id: spectrum_id
        pos: root.pos
        size: root.size
        allow_stretch: True
        pos_size_callback: root._update_cursor  # resize of spectrum image requires updates of cursor
    
    # cursor
    Cursor:
        size_hint: None, None  # set size manually, no resizing by layout binds!
        id: cursor_id
        source: root.cursor_image
        allow_stretch: True
        color: root.cursor_color
    
''')

###############################################################################
def num_to_string(x):
    if x < 9.95:
        return "{:3.1f}".format(x)
    else:
        return "{:>3.0f}".format(x)


def to_si_prefix(x, centi=True):
    if x >= 1e21:
        return (x, "")
    elif x >= 1e18:
        return (x/1e18, "E")
    elif x >= 1e15:
        return (x/1e15, "P")
    elif x >= 1e12:
        return (x/1e12, "T")
    elif x >= 1e9:
        return (x/1e9, "G")
    elif x >= 1e6:
        return (x/1e6, "M")
    elif x >= 1e3:
        return (x/1e3, "k")
    elif x >= 1.:
        return (x, "")
    elif x >= 1e-2 and centi is True:
        return (x/1e-2, "c")
    elif x >= 1e-3:
        return (x/1e-3, "m")
    elif x >= 1e-6:
        return (x/1e-6, "µ")
    elif x >= 1e-9:
        return (x/1e-9, "n")
    elif x >= 1e-12:
        return (x/1e-12, "p")
    elif x >= 1e-15:
        return (x/1e-15, "f")
    elif x >= 1e-18:
        return (x/1e-18, "a")
    else:
        return (x, "")



def format_mantissa_exponent(val):
    # NOTE: int(floor()) only works since ints with magnitude up to 2^24 can be
    # *exactly* represent by IEEE floats. https://en.wikipedia.org/w/index.php?title=Floating_point&oldid=376101741#IEEE_754:_floating_point_in_modern_computers
    e = int(floor(log10(val)))
    m = val / 10.**e
    return "{:.1f} x 10[sup]{}[/sup]".format(m, e)


def format_wavelength(current_wl):
    if current_wl >= 1e6:  # unit prefixes only up to km
        #return "{:.1e} m".format(current_wl)
        return format_mantissa_exponent(current_wl) + " m"
    else:
        r = to_si_prefix(current_wl)
        return "{} {}m".format(num_to_string(r[0]), r[1])


def format_frequency(current_wl):
    c = 299792458.
    f = c / current_wl
    if f >= 1e12:  # unit prefixes only up to GHz
        #return "{:.1e} Hz".format(f)
        return format_mantissa_exponent(f) + " Hz"
    else:
        r = to_si_prefix(f)
        return "{} {}Hz".format(num_to_string(r[0]), r[1])


def format_energy(current_wl):
    c = 299792458.
    h_div_eV = 6.626070e-34 / 1.602177e-19
    E = h_div_eV * c / current_wl
    if E >= 1e12:  # unit prefixes only up to GeV
        #return "{:.1e} eV".format(E)
        return format_mantissa_exponent(E) + " eV"
    else:
        r = to_si_prefix(E, centi=False)
        return "{} {}eV".format(num_to_string(r[0]), r[1])


###############################################################################
class WavelengthIndicator(GridLayout):
    frequency_label = StringProperty("frequency")
    frequency_value = StringProperty("n/a")
    wavelength_label = StringProperty("wavelength")
    wavelength_value = StringProperty("n/a")
    energy_label = StringProperty("energy")
    energy_value = StringProperty("n/a")
    
    #text: "Wavelength λ = " + str(app.root.ids["selector_id"].cursor.current_wl) + "  Frequency ν = 10 Hz"
    
    # TODO: this can do something fancy, such has two boxes with nu and lambda being shown nicely formatted, ....
    # all done by a method, that takes the cursor current_wl as argument (called by cursor on pos change).
    #pass
    

    def set_wavelength(self, current_wl):
        self.frequency_value = format_frequency(current_wl)
        self.wavelength_value = format_wavelength(current_wl)
        self.energy_value = format_energy(current_wl)


    def update(self):
        """Update widget, mainly for language change."""

        self.frequency_label = cfg.content["wavelength_indicator"]["frequency_label"][cfg.lang]
        self.wavelength_label = cfg.content["wavelength_indicator"]["wavelength_label"][cfg.lang]
        self.energy_label = cfg.content["wavelength_indicator"]["energy_label"][cfg.lang]


###############################################################################
class Marker(Image):
    wl = NumericProperty(None)


class Cursor(Image):
    
    current_wl = NumericProperty(1.)
    """Current wavelength (in m)."""
    
    def __init__(self, **kwargs):
        super(Cursor, self).__init__(**kwargs)
        if cfg.verbose > 1:
            print("-- init Cursor done.-- ", self.pos, self.size)

    def on_pos(self, *args):
        cfg.app.root.ids["wli_id"].set_wavelength(self.current_wl)
        


class Spectrum(Image):
    """Spectrum class.
    
    An Image with wavelengths assigned to the x-position.
    """
    
    source = StringProperty("spectrumselector/spectrum2_plain.png")
    """Filename of spectrum image."""
    
    actual_pos = ListProperty([0, 0])
    """Actual position of the texture on the screen (stretched and fitted, as workaround."""
    
    actual_size = ListProperty([100, 100])
    """Actual size of the texture on the screen (stretched and fitted), as workaround.
    Is this accessible somewhere else? self.size isn't the right thing.
    """
    
    spectrum_image_tickvalues = ListProperty([
        { 'xrel': 267/3000., 'wl_m': 1e-14 }, 
        { 'xrel': 2722/3000., 'wl_m': 1e1 }
        ])
    """Defines the correspondence of x-coordinate (going 0...1 left to right of the image) 
    and wavelength (in m) for a logarithmic scale. This needs to be provided for the 
    respective spectrum_image file!
    """
    
    spectrum_image_boundary_xrel = ListProperty([
        123/3000., 2870/3000.,
        ])
    """Left and right boundary of spectrum in xrel (pixel position relative to pixel width)."""
    
    wavelength_markers = ListProperty([])  #([500e-9, 1, 1e-10]))
    """Mark these wavelengths (in m) by showing overlay objects. These are the selection options one can choose."""

    marker_vpos = NumericProperty(0.1)
    """Vertical position of the marker centers, relative to the Image texture height (0...1)."""
    
    pos_size_callback = ObjectProperty(None)
    """If set, this callback will be called if the widget position or size change."""
    
    
    #####################  init
    #
    
    def __init__(self, **kwargs):
        super(Spectrum, self).__init__(**kwargs)
        
        # sort selection list before using it
        self.sort_selection_menu()
        
        # get available wavelenghts / marker positions from provided content variable
        wl_list = []
        for item in cfg.content["selection_menu"]:
            wl_list.append(item["wl_m"])
        if cfg.verbose > 0:
            print("wavelength marker list: {}".format(wl_list))
        self.wavelength_markers = wl_list
        
        self.overlay_markers()
        self.bind(pos=self.update_pos_size, size=self.update_pos_size)
    
    
    def sort_selection_menu(self):
        """Sort cfg.content["selection_menu"] by wavelength before it gets used.
        This allows simple adding of entries without possibly messing up the auto-cycle order.
        """
        cfg.content["selection_menu"].sort(key=lambda x: x["wl_m"])
        
        if cfg.verbose > 1:
            print("sorted list of selections: ")
            for i, item in enumerate(cfg.content["selection_menu"]):
                print("{}. {}".format(i, item["wl_m"]))
    
    
    #####################  widget-related methods
    #
    
    def overlay_markers(self):
        """Draws the prescribed overlay points on the canvas."""
        
        self._markers = []
        for wl in self.wavelength_markers:
            marker = Marker(
                        source="spectrumselector/marker.png",
                        center=self.get_pos(wl, self.marker_vpos),
                        size=[20, 30],
                        allow_stretch=True,
                        keep_ratio=False,
                        wl=wl,
                        color=[0.02, 0.05, 0.07, 1], # tinting the white markers
                    )
            self._markers.append(marker)
            self.add_widget(marker)

    
    def update_pos_size(self, *args):
        """Updates related to position and size changes.
        
        - spectrum image is automatically resized by layout
        - actual_pos and actual_size need to be recalculated:
          they are a wordaround since that information seems not to be available directly...
        - markers need to be repositioned
        """
        
        ### actual_size and actual_pos
        #
        widget_ratio = self.width / float(self.height)
        if self.image_ratio > widget_ratio:
            # image wider: constrained horizontally, centered vertically
            self.actual_size = [self.width, self.norm_image_size[1]]
        else:
            # image narrower: centered horizontally, constrained vertically
            self.actual_size = [self.norm_image_size[0], self.height]
        self.actual_pos = [self.center_x - self.actual_size[0]/2, self.center_y - self.actual_size[1]/2]

        ### markers
        #
        for marker in self._markers:
            marker.center = self.get_pos(marker.wl, self.marker_vpos)
            # bring to foreground - can this be avoided?
            self.remove_widget(marker)
            self.add_widget(marker)
                
        ### external callback if set
        #
        if self.pos_size_callback is not None:
            if cfg.verbose > 1:
                print("Calling Spectrum.pos_size_callback()")
            self.pos_size_callback()
    
    
    def update(self):
        """Update widget, mainly after language change."""
        
        self.source = cfg.content["spectrum_image"][cfg.lang]
        
    
    
    #####################  annotation-related methods
    #
    
    def get_wavelength(self, pos, log=False):
        """Get wavelength (in m) corresponding to pos=[x,y] pixel position."""
        
        xrel = (pos[0] - self.actual_pos[0]) / self.actual_size[0]
        
        # limit to permitted spectrum range:
        if xrel < self.spectrum_image_boundary_xrel[0]:
            xrel = self.spectrum_image_boundary_xrel[0]
        if xrel > self.spectrum_image_boundary_xrel[1]:
            xrel = self.spectrum_image_boundary_xrel[1]
        
        return self.xrel_to_wavelength(xrel, log=log)
    
    
    def get_pos(self, wavelength, yrel=0.5, log=False):
        
        xrel = self.wavelength_to_xrel(wavelength, log=log)
        x = self.actual_pos[0] + xrel * self.actual_size[0]
        
        # y refers to scaled image height
        y = self.actual_pos[1] + yrel * self.actual_size[1]
        return [x, y]
    
    
    def xrel_to_wavelength(self, xrel, log=False):
        """Convert a relative x-position on the spectrum image to a wavelength (in m).
        Just scaling according to the scale ticks provided by spectrum_image_tickvalues.
        
        Parameter "log": True/False whether to return the log10 value of the wavelength.
        """
        
        # abbreviations
        x0 = self.spectrum_image_tickvalues[0]["xrel"]
        x1 = self.spectrum_image_tickvalues[1]["xrel"]
        lwl0 = log10(self.spectrum_image_tickvalues[0]["wl_m"])
        lwl1 = log10(self.spectrum_image_tickvalues[1]["wl_m"])
        
        lwl = (xrel - x0) / (x1 - x0) * (lwl1 - lwl0) + lwl0
        if log is True:
            return lwl
        else:
            return pow(10., lwl)
    
    
    def wavelength_to_xrel(self, wl, log=False):
        """Convert a wavelength (in m) to a relative x-position on the spectrum image.
        Simple scaling.
        
        Parameter "log": True/False whether the value passed in actually is log10 wavelength.
        """
        
        # abbreviations
        x0 = self.spectrum_image_tickvalues[0]["xrel"]
        x1 = self.spectrum_image_tickvalues[1]["xrel"]
        lwl0 = log10(self.spectrum_image_tickvalues[0]["wl_m"])
        lwl1 = log10(self.spectrum_image_tickvalues[1]["wl_m"])
        
        if log is True:
            lwl = wl
        else:
            lwl = log10(wl)
        
        xrel = (lwl - lwl0) / (lwl1 - lwl0) * (x1 - x0) + x0
        return xrel
    


###############################################################################
class SpectrumSelector(FloatLayout):
    """Class for creating an SpectrumSelector."""
    
    cursor_image = StringProperty("spectrumselector/cursor2.png")
    """Filename of cursor image. The marker line needs to be horizontally in the middle (at half width)."""
    
    cursor_image_scale_factor = NumericProperty(1.7)
    """By what factor should the height of the cursor be larger than the spectrum height?"""
    
    cursor_color = ListProperty([0, 0, 0.5, 0.7])
    """Color for the cursor image, used for tinting."""
    
    selection_index = NumericProperty(0)
    """List index of currently active/selected item."""
    
    animation_active = BooleanProperty(False)
    """Cursor motion animated? This is deactivated (False) on startup and during GUI reset, but activated (True)
    during user input."""
    
    # object refs
    cursor = ObjectProperty(None)
    spectrum = ObjectProperty(None)

    #####################  init
    #
    
    def __init__(self, **kwargs):
        super(SpectrumSelector, self).__init__(**kwargs)
        
        #self.bind(pos=self._update_cursor, size=self._update_cursor)
    

    #####################  public interface methods: selection-related
    #
    
    def set_selection(self, ind):
        
        # wrap to normal indices
        ind = ind % len(self.spectrum.wavelength_markers)
        self.selection_index = ind
        
        if cfg.verbose > 1:
            print("Setting new selection ({}) for SpectrumSelector.".format(ind))
            print("Selection is: {}".format(self.spectrum.wavelength_markers))

        wl = self.spectrum.wavelength_markers[ind]
        self.cursor.current_wl = wl
        
        if self.animation_active:
            center = self.spectrum.get_pos(wl, yrel=0.5)
            anim = Animation(
                center=center, 
                t="out_quart", 
                duration=0.3,
                )
            anim.start(self.cursor)
        else:
            self._set_wavelength(wl)
        
        # updates the description and switches on selection change 
        # (since they may depend on the selection context)
        if cfg.app.root is not None:
            cfg.app.root.update_description()
            cfg.app.root.ids["switchpanel_id"].update()   #_on_selection_change()
        
        # notify app (for communications)
        cfg.app.action_state()
        

    
    def select_default(self):
        """Sets (and thus defines) the default selection.
        
        NOTE: Having animation deactivated during startup / select_default: otherwise location set is bad
        (widgets not yet finally repositioned and resized)."""
        
        self.animation_active = False
        self.set_selection(0)
        self.animation_active = True
    
    
    def update(self):
        """Update all child widgets, mainly for language change.
        set text to the one defined by its "tag", depending on language."""

        self._update_cursor()
        self.spectrum.update()
        
        # TODO  to be defined!!!!
        #for index, child in enumerate(self.button_list):
        #    child.text = cfg.content["selection_menu"][index]["label"][cfg.lang]
    
    
    def cycle_forward(self):
        """Change selection to the next one."""
        
        if cfg.verbose > 1:
            print("cycling forward: {} --> {}".format(self.selection_index, self.selection_index+1))
        self.set_selection(self.selection_index + 1)
    
    
    #####################  private methods:
    
    #####################  other selection-related methods
    #

    def _set_wavelength(self, wl, log=False):
        """Set the slider position according to the given wavelength in m."""

        self.cursor.current_wl = wl
        self._update_cursor_pos()
        #anim = Animation(center_x=x, t="out_quart", duration=0.3)
        #anim.start(self.slider)


    def _get_closest_marker_index(self):
        """Finds the index of the marker closest to the current position."""
        
        min_dist = None
        ind = None
        lwl = log10(self.cursor.current_wl)

        # find closest marker in log space
        for i, mark_wl in enumerate(self.spectrum.wavelength_markers):
            d = abs(lwl - log10(mark_wl))
            if (min_dist is None) or (d < min_dist):
                min_dist = d
                ind = i

        #print "lwl={}  min_dist={}  ind={}".format(lwl, min_dist, ind)
        return ind


    #####################  cursor methods
    #
    
    def _update_cursor(self, *args):
        #print("_update_cursor: actual_size={}  size={}".format(self.spectrum.actual_size, self.spectrum.size))
        self._update_cursor_size()
        self._update_cursor_pos()
    
    
    def _update_cursor_size(self):
        self.cursor.height = self.spectrum.actual_size[1] * self.cursor_image_scale_factor
        self.cursor.width = self.cursor.height * self.cursor.image_ratio
        if cfg.verbose > 2:
            print("_update_cursor_size: cursor.size={}, spectrum.actual_size[1]*factor={}".format(
                self.cursor.size, 
                self.spectrum.actual_size[1] * self.cursor_image_scale_factor
                ))
                
        
    def _update_cursor_pos(self):
        self.cursor.center = self.spectrum.get_pos(self.cursor.current_wl, yrel=0.5)
        if cfg.verbose > 2:
            print("_update_cursor_pos: cursor.center={}  current wl={}".format(self.cursor.center, self.cursor.current_wl))
    
    
    #####################  touch-related methods
    #
    
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            if self.animation_active:
                Animation.cancel_all(self.cursor)
            self._set_cursor_on_touch(touch)
            return True
    
    
    def on_touch_move(self, touch):
        if self.collide_point(*touch.pos):
            self._set_cursor_on_touch(touch)
            return True
    
    
    def on_touch_up(self, touch):
        # no collide_point conditional here: #if self.collide_point(*touch.pos):
        #self._set_cursor_on_touch(touch)
        ind = self._get_closest_marker_index()
        self.set_selection(ind)
        return True
    
    
    def _set_cursor_on_touch(self, touch):
        self.cursor.current_wl = self.spectrum.get_wavelength(touch.pos)
        self._update_cursor_pos()
        #print("-- touch: {}   wl={:e}".format(touch.pos, self.cursor.current_wl))



###############################################################################
if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            layout = BoxLayout(orientation='horizontal')
            layout.add_widget(Button(text='left', size_hint_x=0.1))
            layout.add_widget(SpectrumSelector())
            layout.add_widget(Button(text='right', size_hint_x=0.1))
            return layout
        
    SampleApp().run()


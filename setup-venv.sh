#!/bin/sh

test -d venv && { echo "Virtual environment already present. Exit."; exit; }

echo "Setting up virtual environment for python..."
virtualenv -p /usr/bin/python2 venv
. venv/bin/activate

# force Cython install before requirements
pip install Cython==0.21.1
# other requirements
pip install -r requirements.txt

deactivate


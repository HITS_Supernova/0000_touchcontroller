from kivy.uix.image import Image
from kivy.uix.behaviors import ToggleButtonBehavior
from udobject import UdObject

class ToggleSwitch(ToggleButtonBehavior, Image, UdObject):
    """ToggleSwitch implements a on/off-switch with Android design

    Implemented with ToggleButtonBehavior, so self.state instead of self.active
    TODO: images should go into an atlas.
    UDObject inherits the "ud" user dictionary to it, where custom data can be stored
    """

    def __init__(self, theme='dark', **kwargs):
        super(ToggleSwitch, self).__init__(**kwargs)
        
        # images
        self.source_on = 'toggleswitch/light_on.png'
        self.source_off = 'toggleswitch/light_off.png'
        self.source = self.source_off
        
    def on_state(self, widget, value):
        if value == 'down':
            self.source = self.source_on
        else:
            self.source = self.source_off



if __name__ == "__main__":

    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return ToggleSwitch()

    SampleApp().run()


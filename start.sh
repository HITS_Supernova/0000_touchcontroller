#!/bin/sh
# start main.py using a virtual environment

# path to virtual environment, with default value
PYTHON_VENV=${PYTHON_VENV:-venv}

wd="$( dirname $(readlink -e $0) )"
cd "$wd"

activate="$PYTHON_VENV/bin/activate"
if [ -r "$activate" ] ; then
    . "$activate"
    exec python main.py
else
    echo "Error: virtual environment '$PYTHON_VENV' not found. Please create one with setup-venv.sh."
    exit 1
fi

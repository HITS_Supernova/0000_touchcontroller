"""
    ImageToggleButton
    =================

    The ImageToggleButton widget creates a component with a background image and
    foreground text, that is clickable as a ToggleButton.

    With a group being used, this behaves like a set of radio buttons: exactly one is active at all times
    on_press action makes sure that deactivation only happens by selecting another on

    * How to use in kv file:

    ImageToggleButton:
        font_size: '25sp'

"""

__all__ = ('ImageToggleButton')
__version__ = '0.1'


from kivy.lang            import Builder
from kivy.uix.behaviors   import ToggleButtonBehavior
from kivy.uix.image       import Image
from kivy.properties      import StringProperty, ListProperty,\
                                 NumericProperty, BooleanProperty,\
                                 ObjectProperty
from udobject             import UdObject



Builder.load_string('''

<ImageToggleButton>:
    #size_hint: None, None  # ?????

    canvas.after:
        Color:
            rgba: root.outline_color
        Line:
            points: self.x, self.y, self.right, self.y, self.right, self.top, self.x, self.top
            width: root.outline_width
            cap: 'none'
            joint: 'round'
            close: True
    Label:
        canvas.before:
            Color:
                rgba: root.box_color
            Rectangle:
                pos: root.pos
                size: root.width, root.box_height * root.height
        pos: root.pos
        size: root.width, root.box_height * root.height
        text: "[b]" + root.text + "[/b]"
        color: root.text_color
        font_size: root.font_size
        markup: True
''')

class ImageToggleButton(ToggleButtonBehavior, Image, UdObject):
    """Class for creating an ImageToggleButton."""
    
    text = StringProperty("")
    """Text in widget foreground (label)."""
    
    source = StringProperty("")
    """Filename of background image."""
    
    font_size = NumericProperty('15sp')
    """Font size of the labels."""
    
    image_tint_down = ListProperty([1, 1, 1, 1])
    """rgba color list used for image 'tinting' when selected."""
    
    image_tint_normal = ListProperty([1, 1, 1, 0.3])
    """rgba color list used for image 'tinting' when *not* selected."""
    
    outline_width = NumericProperty(5)
    """Width of outline (if selected), in pixels."""
    
    outline_color = ListProperty([0.5, 0.5, 0.5, 0])
    """rgba color of the outline (if selected)."""
    
    text_color = ListProperty([1, 1, 1, 1])
    """rgba color of the label text."""
    
    keep_ratio = BooleanProperty(False)
    """Keep aspect ratio for the background image."""
    
    allow_stretch = BooleanProperty(True)
    """Allow stretching of background image beyond its original resolution (magnify)?"""
    
    box_color = ListProperty([0, 0, 0, 0.5])
    """rgba color for gray box put behind the text label."""
    
    box_height = NumericProperty(0.3)
    """Height of the text background box, relative to overall widget height."""
    
    tag = StringProperty("")
    """Custom tag string to be attached to the widget (identifying its content)."""
    
    allow_no_selection = BooleanProperty(False)
    """Radio button behavior (from ToggleButtonBehavior)."""
    
    pressed_callback = ObjectProperty(None)
    """Reference to callback method if activated"""
    
    # private:
    _image_tint = ListProperty([1, 1, 1, 1])
    """current rgba list for image tinting. See Image.color for this."""
    

    def on_state(self, instance, value):
        if value == "down":
            self.outline_color[3] = 1
            self.color = self.image_tint_down
            if self.pressed_callback is not None:
                self.pressed_callback(self)
        else:
            self.outline_color[3] = 0
            self.color = self.image_tint_normal



if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return ImageToggleButton(text="Text", source="HalfSphere/01_Earth.jpg", group="grp")

    SampleApp().run()


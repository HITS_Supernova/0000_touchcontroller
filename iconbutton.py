"""
    IconButton
    ==========



"""

__all__ = ('IconButton')
__version__ = '0.1'


from kivy.lang            import Builder
from kivy.uix.behaviors   import ButtonBehavior
from kivy.uix.image       import Image
from kivy.properties      import StringProperty, ListProperty,\
                                 BooleanProperty, NumericProperty
from udobject             import UdObject



Builder.load_string('''

<IconButton>:
    # colored canvas for debugging:
    #canvas.before:
    #    Color:
    #        rgba: 1, 0, 0, 1
    #    Rectangle:
    #        pos: self.pos
    #        size: self.size
            
    Label:
        size: root.size
        pos: root.pos
        text: "[b]" + root.text + "[/b]"
        color: root.text_color
        font_size: root.font_size
        markup: True
''')

class IconButton(ButtonBehavior, Image, UdObject):
    """Class for creating an IconButton."""
    
    text = StringProperty("")
    """Text in widget foreground (label)."""
    
    font_size = NumericProperty('15sp')
    """Font size of the labels."""
    
    source = StringProperty("")
    """Filename of background image."""
    
    source_normal = StringProperty("iconbutton/icon-normal.png")
    """Filename of background image, not pressed."""
    
    source_down = StringProperty("iconbutton/icon-down.png")
    """Filename of background image, pressed."""

    image_tint_down = ListProperty([1, 1, 1, 1])
    """rgba color list used for image 'tinting' when selected."""
    
    image_tint_normal = ListProperty([1, 1, 1, 1])
    """rgba color list used for image 'tinting' when *not* selected."""
    
    text_color = ListProperty([1, 1, 1, 1])
    """rgba color of the label text."""
    
    keep_ratio = BooleanProperty(True)
    """Keep aspect ratio for the background image."""
    
    allow_stretch = BooleanProperty(True)
    """Allow stretching of background image beyond its original resolution (magnify)?"""
    
    
    def __init__(self, **kwargs):
        super(IconButton, self).__init__(**kwargs)
        self.source = self.source_normal
        
    def on_state(self, instance, value):
        if value == "down":
            self.source = self.source_down
            self.color = self.image_tint_down
        else:
            self.source = self.source_normal
            self.color = self.image_tint_normal



if __name__ == "__main__":
    from kivy.app import App

    class SampleApp(App):
        def build(self):
            return IconButton(text="Text")

    SampleApp().run()


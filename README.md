# 0000 - Touch Controller

## Short Description

This is a helper application with touch input, used to control other graphical applications via 
websockets, specifically the [Projection on a Half Sphere](https://gitlab.com/HITS_Supernova/0202_halfsphere) 
and the [Milky Way Panorama](https://gitlab.com/HITS_Supernova/1007_milkywaypanorama).

This application is used at the [ESO Supernova Planetarium and Visitor Centre](https://supernova.eso.org/?lang=en), Garching b. München.  
For more details about the project and how applications are run and managed within the exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).   

## Requirements / How-To

Hardware: a touch screen device is needed for user input. The layout was optimized for a resolution of 1280 x 800 pixels.

Software: you need Python 2.7 and several modules installed (e.g. via `pip`), particularly Kivy and Twisted. 
See [`requirements.txt`](requirements.txt) for the full list.

## Detailed Information

The application is able to serve different content and have a different graphical user interface. 
It is implemented with the [Kivy](https://kivy.org) framework in Python, where specific settings are defined
in a yaml file and the user interface is defined by a kv file (the yaml file contains the reference to this kv file).

The name of the yaml file to be used should be set by an environment variable `TOUCH_CONTROLLER_YAML`. 
For the _Half Sphere Projection_ use `export TOUCH_CONTROLLER_YAML=content_earth_moon.yaml`, 
for the _Milky Way Panorama_ use `export TOUCH_CONTROLLER_YAML=content_milky_way.yaml`.

The application is started with `python main.py` and runs a websocket server on TCP port `8889`
and pushes any selection commands to the connected clients (with the definitions from the yml file).
These and several other configuration settings can me made in `cfg.py`.

Projection on a Half Sphere:

- `content_earth_moon.yml`
- `tc_earth_moon.kv`

Milky Way Panorama:

- `content_milky_way.yaml`
- `tc_milky_way.kv`

## Credits and License

Developed by Volker Gaibler, HITS gGmbH. This repository is licensed under the [MIT license](LICENSE).

#### Image Credits

* ToggleSwitch image files: derived from 
  Google [Material design](https://material.google.com/components/selection-controls.html), Apache 2.0 license.

* Icons in `Halfsphere/`:
  derived from our `0202_halfsphere` repository (MIT license).
	


import cfg
from twisted.python import log

from autobahn.twisted.websocket import WebSocketServerProtocol

###############################################################################
class WSProtocol(WebSocketServerProtocol):
    #log = Logger()
    
    def onConnect(self, request):
        log.msg("Client connecting: {0}".format(request.peer))
        log.msg("connect: {}".format(request))
        #dict_print(request)
        log.msg("currently connected clients: {}".format(self.factory.getConnectionCount()))
        
        
    def onOpen(self):
        log.msg("WebSocket connection open.")
        
        # allow only one connection, and store a reference
        #if cfg.conn is None:
        #    cfg.conn = self
        
        # save reference to last incoming connection
        # last one connected will receive the commands (needed if reconnect leaves old conn open for a while)
        cfg.conn = self
        
        cfg.app.set_conn_status(cfg.footer_text_connected)
        
        # send full state information, to be surely in sync
        cfg.app.send_state()
            
        #else:
        #    msg = "Sorry. Busy with another client..."
        #    self.sendMessage(msg, isBinary=False)
        #    log.msg(msg)
        #    # self.sendClose()
        #    self.sendClose(code=3000, reason=u"Busy")
        
        
    def onMessage(self, payload, isBinary):
        """
        if isBinary == False: converted to utf8 
        --> s.decode('utf8')
        --> s.encode('utf8')
        warn: websocket peer will close if invalid utf8 encountered.
        
        json decode: obj = json.loads(payload.decode('utf8'))
        json encode: payload = json.dumps(obj, ensure_ascii=False).encode('utf8')
        """
        if isBinary:
            log.msg("Binary message received: {0} bytes".format(len(payload)))
        else:
            log.msg("Text message received: {0}".format(payload.decode('utf8')))
        
        # echo back message verbatim
        #self.sendMessage(payload, isBinary)
        
        
    def onClose(self, wasClean, code, reason):
        log.msg("WebSocket connection closed: {0} [{1}]".format(reason, wasClean))
        
        cfg.app.set_conn_status(cfg.app.hostinfo)


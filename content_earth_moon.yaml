
###############################################################################
# GUI layout by kv file
#
kv_file: "tc_earth_moon.kv"


###############################################################################
header:
    # Title in header
    label:
        en: Earth and Moon in motion
        de: Erde und Mond in Bewegung



selection_menu:
  # This lists all the available items shown in SelectionMenu with their 
  # corresponding background image file paths, text labels and descriptive text
  #
  #############################################################################
  - tag: Earth
    icon: HalfSphere/01_Earth.jpg
    texture: Images/earth/ColorMapb2.jpg
    #also possible: URLs
    label:
        en: Earth
        de: Erde
    description:
        en: |
            [b]Satellite view of the Earth[/b]
            
            Weather satellites usually are on a geostationary orbit around the Earth. Since the orbital period there is exactly one day, these satellites seem to stand still above the equator and can observe the weather continuously.
            
            Credit: R. Stöckli, NASA Earth Observatory
        de: |
            [b]Satellitenansicht der Erde[/b]
            
            Die Wettersatelliten befinden sich meistens in einer geostationären Umlaufbahn um die Erde. Da hier die Umlaufzeit genau einen Tag beträgt, stehen diese Satelliten über dem Äquator scheinbar still und können das Wettergeschehen laufend beobachten.
            
            Credit: R. Stöckli, NASA Earth Observatory
    overlay_grid: &overlay_grid_earth
        label:
            en: Coordinate system
            de: Koordinatensystem
        # Note on naming:
        # switch is on: button "down", switch is off: button "normal"
        on_texture:
            en: Images/overlays/Earth_grid_EN.png
            de: Images/overlays/Earth_grid_DE.png
        off_texture:
            en: Images/overlays/empty.png
            de: Images/overlays/empty.png
  #############################################################################
  - tag: EarthNight
    icon: HalfSphere/02_EarthNight.jpg
    texture: Images/earth/NightLights.jpg
    label:
        en: Night
        de: Nacht
    description: 
        en: |
            [b]Earth at night[/b]
            
            The night view of the Earth clearly shows the densely populated areas on Earth due to the pervasive illumination. “Light pollution” is also responsible for the fact that the dark night sky often is only observable in remote areas.
            
            Credit: C. Mayhew/R. Simmon/NASA GSFC
        de: |
            [b]Die Erde bei Nacht[/b]
            
            Eine Nachtaufnahme der Erde zeigt klar die Ballungszentren der Erde, die durch die allgegenwärtige Beleuchtung klar hervortreten. Als “Lichtverschmutzung” führt sie aber auch dazu, dass der dunkle Nachthimmel oft nur noch in entlegenen Gebieten beobachtbar ist.
            
            Credit: C. Mayhew/R. Simmon/NASA GSFC
    overlay_grid: *overlay_grid_earth
            
  #############################################################################
  - tag: EarthTemperature
    icon: HalfSphere/03_EarthTemperature.png
    texture: Images/video/surf1.m4v
    label: 
        en: Temperature
        de: Temperatur
    description:
        en: |
            [b]Surface temperature of the Earth (video)[/b]
            
            GEOS-5 is a collection of climate models of the NASA Center for Climate simulation which examines the processes leading to long-term changes of weather and climate. Shown here is the change of surface temperature in the “Nature Run”. The cold to warm colors show temperatures of -3 °C to 37°C, and white color indicates the outgoing longwave radiation in the atmosphere due to the clouds.
            
            Credit: W. Putman/NASA/GSFC
        de: |
            [b]Oberflächentemperatur der Erde (Video)[/b]
            
            GEOS-5 ist eine Sammlung von Klimasimulationen des NASA Center for Climate Simulation, das Prozesse untersucht, die zu langfristigen Wetter- und Klimaveränderungen führen. Für den “Nature Run” wird hier die modellierte Änderung der Oberflächentemperatur der Erde im Tageslauf gezeigt. Die kalten bis warmen Farben zeigen dabei Temperaturen von -3 °C bis 37 °C an, weiß zeigt die langwellige Abstrahlung in der Atmosphäre durch Wolken an.
            
            Credit: W. Putman/NASA/GSFC
    overlay_grid: *overlay_grid_earth
    
  #############################################################################
  - tag: EarthElevation
    icon: HalfSphere/04_EarthElevation.jpg
    texture: Images/earth/Elevation1.jpg
    label: 
        en: Elevation
        de: Höhenrelief
    description: 
        en: |
            [b]Terrain map of the continents and oceans[/b]
            
            The color-coded terrain map shows both the continents and the oceans of the Earth. Not only the large continental mountain ranges are prominently visible - the topography of the ocean floor is clearly visible, too. The lowest oceanic regions (-11 km) are shown in dark blue, the hightest elevations in red (5 km) and gray (above 5 km).
            
            Credit: ETOPO2v2/NOAA
        de: |
            [b]Höhenrelief der Kontinente und Ozeane[/b]
            
            Die farbcodierte Höhenkarte zeigt sowohl die Kontinente als auch die Ozeane der Erde. Auf ihr stechen nicht nur die großen kontinentalen Gebirgszüge in Auge, auch die Topographie des Ozeanbodens ist deutlich erkennbar. Die tiefsten Ozeanbereiche (-11 km) sind dunkelblau, die höchsten Erhebungen rot (5 km) und grau (über 5 km) dargestellt.
            
            Credit: ETOPO2v2/NOAA
    overlay_grid: *overlay_grid_earth
    
  #############################################################################
  #- tag: EarthEqTectonic
  #  icon: HalfSphere/05_EarthEqTectonic.jpg
  #  texture: Images/earth/Elevationh.jpg
  #  label: 
  #      en: Earthquakes
  #      de: Erdbeben
  #  description: 
  #      en: |
  #          [b]Terrain map including tectonic plates and earthquakes[/b]
  #          
  #          In addition to the terrain map of the Earth, this view shows the tectonic plates (in black) and the epicenters of earthquakes between 1900 and 2007 with magnitudes above 6. It is evident that the latter are preferrably located at the rims of tectonic plates.
  #          
  #          Credit: Centennial Earth Quake Catalog/USGS/Dagik
  #      de: |
  #          [b]Höhenrelief mit tektonischen Platten und Erdbeben[/b]
  #          
  #          In dieser Reliefkarte der Erde sind zusätzlich die tektonischen Platten (schwarz) eingezeichnet sowie die Epizentren der Erdbeben zwischen 1900 und 2007 mit Magnituden über 6. Es ist klar zu erkennen, dass diese bevorzugt am Rand tektonischer Platten auftreten.
  #          
  #          Credit: Centennial Earth Quake Catalog/USGS/Dagik
  #  overlay_grid: *overlay_grid_earth
    
  #############################################################################
  - tag: EarthWind
    icon: HalfSphere/06_EarthWind.png
    texture: Images/video/wind1.m4v
    label: 
        en: Wind
        de: Wind
    description: 
        en: |
            [b]Global air circulation (video)[/b]
            
            Air flows distribute aerosoles in the atmosphere across the globe. For the GEOS-5 simulation the winds on the surface are shown here in white (up to 140 km/h), and the winds at higher altitudes in color (up to 630 km/h, in red).
            
            Credit: W. Putman/NASA/GSFC
        de: |
            [b]Globale Windströmungen (Video)[/b]
            
            Windströmungen verteilen Aerosole in der Atmosphäre und über den Planeten. Für die obige GEOS-5-Simulation werden hier die Oberflächenwinde in weiß dargestellt (bis zu 140 km/h), die Winde in größeren Höhen in Farbe (bis 630 km/h, rot).
            
            Credit: W. Putman/NASA/GSFC
    overlay_grid: *overlay_grid_earth
    
  #############################################################################
  - tag: EarthAerosols
    icon: HalfSphere/07_EarthAerosols.png
    texture:  Images/video/aero1.m4v
    label: 
        en: Aerosols
        de: Aerosole
    description: 
        en: |
            [b]Aerosoles in the atmosphere (video)[/b]
            
            Aerosoles are tiny airborne particles that play an important role both for weather and the climate. Air motion transports them around the globe, so they get spread out to regions far from their origin. Shown here for the GEOS-5 simulations is the spreading of aerosoles from dust, fires, fossil fuels and volcanos. The colors indicate the different aerosoles that were modeled: dust (red-orange), sea salt (blue), black and organic carbon (green) and sulfates with ash-brown to white. Wild fires and human-initiated burning are included, too (yellow and red dots on the land surface).
            
            Credit: W. Putman/NASA/GSFC
        de: |
            [b]Aerosole in der Atmosphäre (Video)[/b]
            
            Aerosole sind kleinste Schwebeteilchen in der Atmosphäre. Sie spielen für das Wetter als auch für das Klima eine wichtige Rolle. Mit den Luftmassen werden sie quer über Globus transportiert, weit entfernt von ihrem Entstehungsort. Für die GEOS-5-Simulationen wird hier die Ausbreitung von Aerosolen durch Staub, Brände, fossile Energieträger und Vulkane dargestellt. Die Farben stellen dabei die unterschiedlichen modellierten Aerosole dar: Staub (rot-orange), Meersalz (blau), Kohle (grün) und Sulfate (weiß). Auch Waldbrände und Brandrodung sind hier mit eingeschlossen (gelbe und rote Punkte an Land).
            
            Credit: W. Putman/NASA/GSFC
    overlay_grid: *overlay_grid_earth
    
  #############################################################################
  - tag: EarthRain
    icon: HalfSphere/08_EarthRain.png
    texture: Images/video/rain1.m4v
    label: 
        en: Rain
        de: Niederschläge
    description: 
        en: |
            [b]Precipitation (video)[/b]
            
            Shown here from the GEOS-5 simulation is the total mass of water vapor in the atmosphere (white) and the precipitation (in color, red corresponds to high values of 15 mm/hour and more).
            
            Credit: W. Putman/NASA/GSFC
        de: |
            [b]Niederschläge (Video)[/b]
            
            Aus der GEOS-5-Simulation werden hier die Gesamtwasserdampfmasse in der Atmosphäre (weiß) und die Niederschläge gezeigt (farbig, rot entspricht hohen Werten von 15 mm/Stunde und mehr).
            
            Credit: W. Putman/NASA/GSFC
    overlay_grid: *overlay_grid_earth
    
  #############################################################################
  - tag: EarthCO2
    icon: HalfSphere/09_EarthCO2.png
    texture: Images/video/co2_1.m4v
    label: 
        en: CO[sub]2[/sub]
        de: CO[sub]2[/sub]
    description: 
        en: |
            [b]CO[sub]2[/sub] (video)[/b]
            
            The “Nature Run” of the GEOS-5 climate simulation is based on actually measured atmopheric data, greenhouse gas emission and dust emission of natural and human origin. Based on these, the further evolution (January to December 2006) is computed. Shown here is the distribution of carbon dioxide (CO[sub]2[/sub]) in the atmosphere - how CO[sub]2[/sub] plumes swirl around, originating from their sources and getting spread all over the planet. The simulation shows the different CO[sub]2[/sub] concentration on the northern and southern hemisphere, too, as well as the impact of the growing season during a year.
            
            Credit: W. Putman/NASA/GSFC
        de: |
            [b]CO[sub]2[/sub] (Video)[/b]
            
            Der “Nature Run” der GEOS-5-Klimasimulation basiert auf tatsächlich gemessenen Atmosphärendaten, Treibhausgas-Emissionen und natürlichen wie menschengemachten Staubemissionen. Daraus wird dann die weitere Entwicklung berechnet (Januar bis Dezember 2006). Hier wird die Verteilung von Kohlenstoffdioxid (CO[sub]2[/sub]) in der Atmosphäre dargestellt - wie CO[sub]2[/sub]-Fahnen ausgehend von ihren Quellen herumgewirbelt werden und das um den ganzen Globus bewegen. Die Simulation zeigt auch die unterschiedliche CO[sub]2[/sub]-Konzentration auf der Nord- und Südhalbkugel sowie Änderungen durch den Einfluß des Vegetationszyklus während eines Jahres.
            
            Credit: W. Putman/NASA/GSFC
    overlay_grid: *overlay_grid_earth
    
  #############################################################################
  - tag: Moon
    icon: HalfSphere/10_Moon.png
    texture: Images/moon/LOLA_plus_Clementine_45r_4K_cc.png
    label: 
        en: Moon
        de: Mond
    description:
        en: |
            [b]The Moon[/b]

            The Moon orbits the Earth every 27.3 days. Due to tidal locking, the Moon's rotation period is also 27.3 days, so it always keeps the same face towards the Earth. The far side of the Moon had never been seen before the dawn of the space age. The Moon has no water, no air, and very little (if any) geological activity. As a result, surface markings like impact craters and Apollo astronaut boot prints remain unchanged for thousands of years.
        de: |
            [b]Der Mond[/b]

            Der Mond umkreist die Erde 27,3 Tagen. Auch für die Umdrehung um seine eigene Achse braucht er 27,3 Tage. Aufgrund der sogenannten gebundenen Rotation ist seine Rotationsperiode an seine Umlaufperiode um die Erde gekoppelt. Darum sehen wir von der Erde aus auch immer die gleiche Seite des Mondes. Die von der Erde abgewandte Seite können wir erst seit Beginn des Raumfahrtzeitalters beobachten. Auf dem Mond gibt es weder Wasser noch Luft und nur geringe geologische Aktivität. Krater von Einschlägen und die Fußabdrücke der Apollo-Astronauten bleiben daher für Tausende von Jahren erhalten. 
    overlay_grid: &overlay_grid_moon
        label:
            en: Coordinate system
            de: Koordinatensystem
        # Note on naming:
        # switch is on: button "down", switch is off: button "normal"
        on_texture:
            en: Images/overlays/Moon_grid_EN.png  # TODO:
            de: Images/overlays/Moon_grid_DE.png
        off_texture:
            en: Images/overlays/empty.png
            de: Images/overlays/empty.png



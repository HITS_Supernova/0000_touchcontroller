#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Touch Controller

on desktop:
  python main.py -m inspector  --size 1280x800

to build for android:
- [buildozer init]
- buildozer android debug deploy
- adb logcat -s python
- and then run.....

"""

from __future__ import print_function, unicode_literals
from __future__ import division


# install_twisted_rector must be called before importing  and using the reactor
from kivy.support import install_twisted_reactor
install_twisted_reactor()

from twisted.internet import reactor
from twisted.python import log   # info: log.msg() and error: log.err()
from autobahn.twisted.websocket import WebSocketServerFactory

import time
import sys

import json

import kivy
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from kivy.metrics import sp, dp
from kivy.uix.behaviors import ToggleButtonBehavior
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.clock import Clock

# my own switch
from toggleswitch import ToggleSwitch
from iconbutton import IconButton
from imagetogglebutton import ImageToggleButton
from wsprotocol import WSProtocol
from spectrumselector import SpectrumSelector


import gettext

kivy.require('1.9.0')

# local configuration
try:
    import cfg
except ImportError:
    print("Could not import cfg.")

# version string
__version__ = "0.1"



###############################################################################
# helper functions
#
def get_ip_address():
    """Determine our own IP address by connecting to an examplary host."""
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("www.h-its.org", 80))
    return s.getsockname()[0]


###############################################################################
class SelectionMenu(GridLayout):
    """
    Information about configuration (from YAML) is stored in self.ud
    """
    
    selection_index = NumericProperty(0)
    """List index of currently active/selected item."""
    

    def __init__(self, **kwargs):
        super(SelectionMenu, self).__init__(**kwargs)
        
        # maintain our own sorted list of entries 
        # instead of relying on the (reverse) ordering of "self.children"
        self.button_list = []
        
        group = "selection_menu"
        for idx, item in enumerate(cfg.content["selection_menu"]):
            #child = Button(text=item["tag"], color=[0, 0, 0, 1])
            child = ImageToggleButton(
                group=group, 
                state="normal", 
                source=item["icon"], 
                size_hint_y=0.9, 
                text="",
                color=[1, 1, 1, 0.3],
                outline_color=[0, 0, 0, 0],
                pressed_callback=self._pressed_callback, # on pressed, button notifies its parent SelectionMenu widget
                )
            child.ud["idx"] = idx  # save identity (index)
            self.add_widget(child)
            self.button_list.append(child)
        
        # enable the first one on startup
        self.select_default()
        
        
    ### public interface:
    #
    def set_selection(self, idx):
        """Sets the selected element of the SelectionMenu, identified by its index.
        This is called either from outside (to trigger a change) or by the selected button though a callback.
        """
        
        if cfg.verbose > 1:
            print("Setting new selection ({}) for SelectionMenu.".format(idx))
        
        self.selection_index = idx % len(self.button_list)
        self.button_list[self.selection_index].trigger_action(0)
        
        # updates the description and switches on selection change (since they may depend on the selection context)
        if cfg.app.root is not None:
            cfg.app.root.update_description()
            cfg.app.root.ids["switchpanel_id"].update()
        
        # notify app (for communications)
        cfg.app.action_state()
        
        
    def select_default(self):
        """Sets (and thus defines) the default selection."""
        
        self.set_selection(0)
        
        
    def update(self):
        """Update all child widgets, mainly for language change.
        set text to the one defined by its "tag", depending on language."""

        for index, child in enumerate(self.button_list):
            child.text = cfg.content["selection_menu"][index]["label"][cfg.lang]
        
        
    def cycle_forward(self):
        """Change selection to the next one."""
        
        if cfg.verbose > 1:
            print("cycling forward: {} --> {}".format(self.selection_index, self.selection_index+1))
        self.set_selection(self.selection_index + 1)
    
    
    ### private methods
    #
    def _pressed_callback(self, widget):
        """Callback notifies parent (self) when one of the buttons is pressed."""
        
        self.set_selection(widget.ud["idx"])
    
    
    def _get_active_button(self):
        return self.button_list[self.selection_index]



###############################################################################
class SwitchPanel(GridLayout):
    """Panel of on/off switches with labels.
    
    Currently ugly implementation with empty widgets left and right
    to bring them closer together. Better done with alignment about the center line.
    """

    def __init__(self, **kwargs):

        super(SwitchPanel, self).__init__(**kwargs)

        # determine number of switches (defined by first selection_menu element)
        switch_keys = [ s for s in cfg.content["selection_menu"][0].keys() if s.startswith("overlay_")]
        
        # keep references to switches (subset of children)
        self.switch_list = []
        
        # set rows of GridLayout
        self.rows = len(switch_keys)
        
        #for item in cfg.content["switches"]:
        for key in switch_keys:
            #print("*** item with tag {}: {}".format(item["tag"], item))
            child = Widget(
                size_hint_x=0.05,
                )  # empty
            self.add_widget(child)
            
            child_label = Label(
                text="n/a",
                size_hint_x=0.7,
                color=cfg.switch_label_color,
                #halign="right",
                #valign="middle",
                #text_size=[2*self.width, self.height],
                )
            self.add_widget(child_label)
            
            child_switch = ToggleSwitch(
                color=cfg.switch_color,
                size_hint_x=0.2,
                state="normal",
                disabled=cfg.switch_disabled,
                )
            child_switch.ud["labelwidget"] = child_label  # keep reference to corresponding label for text update
            child_switch.ud["key"] = key
            child_switch.bind(state=cfg.app.action_state)  # on change, notify the app
            self.switch_list.append(child_switch)
            self.add_widget(child_switch)
            
            child = Widget(
                size_hint_x=0.05,
                )  # empty
            self.add_widget(child)

    
    def update(self):
        """Update widgets, caused by change of selection or language."""
        
        
        if cfg.app.root is not None:
            si = cfg.app.root.ids["selector_id"].selection_index

            for switch in self.switch_list:
                key = switch.ud["key"]
                switch.ud["labelwidget"].text = cfg.content["selection_menu"][si][key]["label"][cfg.lang]

        #for switch in self.switch_list:
            #switch.ud["labelwidget"].text = switch.ud["label"][cfg.lang]
        #print("SwitchPanel update called.")


    def select_default(self):
        for switch in self.switch_list:
            #print("switch", switch.ud["tag"])
            switch.state = "normal"


###############################################################################
class TCRoot(FloatLayout):
    """The root widget"""
        
    statusbar_text = StringProperty("")
    
    def __init__(self, **kwargs):

        # widget init
        super(TCRoot, self).__init__(**kwargs)
        
        self.last_activity = time.time()
        #print("TCRoot __init__ done.")

        
    def update_selection(self):
        """Update only the selection menu."""
        
        si = self.ids["selector_id"].update()
        
        
    def update_description(self):
        """Update only the description field."""
        
        si = self.ids["selector_id"].selection_index
        self.ids["description_id"].text = cfg.content["selection_menu"][si]["description"][cfg.lang]
        
        
    def update_switches(self):
        """Update only the switches."""
        
        self.ids["switchpanel_id"].update()
        
        
    def update_all(self):
        """Update all widgets to current settings, particularly language."""
        
        # header
        self.ids["header_id"].text = cfg.content["header"]["label"][cfg.lang]
        
        # button shows *next* language:
        idx = (cfg.languages_available.index(cfg.lang) + 1) % len(cfg.languages_available)
        self.ids["languagebutton_id"].text = cfg.languages_available[idx].upper()
        
        # selection menu
        self.update_selection()
        
        # switches
        self.update_switches()
        
        # description
        self.update_description()
        
        # wavelength indicator if present
        if "wli_id" in self.ids:
            self.ids["wli_id"].update()
        

    def reset_gui(self):
        """Reset GUI to defaults."""
        
        # reset menu to default [0]
        # reset switches to off
        if cfg.verbose > 1:
            print("Resetting GUI...")
        self.ids["selector_id"].select_default()
        self.ids["switchpanel_id"].select_default()
        self.update_all()

    def cycle_forward(self, *args):
        """Do next step, cycling through the selection menu. This should be triggered after some inactive time..."""
        self.ids["selector_id"].cycle_forward()


    def on_touch_down(self, touch):
        cfg.app.set_last_activity()
        super(TCRoot, self).on_touch_down(touch)
        

    def on_touch_move(self, touch):
        cfg.app.set_last_activity()
        super(TCRoot, self).on_touch_move(touch)
        return False


    def on_touch_up(self, touch):
        
        # reset inactivity timer
        cfg.app.set_last_activity()
        
        # call "super" to let normal touch bubbling continue (to avoid SpectrumSelector missing the touch)
        # https://kivy.org/docs/api-kivy.uix.widget.html#widget-touch-event-bubbling
        super(TCRoot, self).on_touch_up(touch)
        return False


###############################################################################
class TCApp(App):

    def __init__(self, **kwargs):
        super(TCApp, self).__init__(**kwargs)
        
        # attributes
        self._last_activity = time.time()
        
        # keep kivy event for auto-cycling if active (None otherwise)
        self.auto_cycle_event = None
        self.inactivity_check_event = None
    
    
    ### app init
    #
    def build(self):
        
        log.startLogging(sys.stdout)
        
        # allow global references to app, without parameter passing or using App.get_running_app()
        cfg.app = self
        
        # make sure that reference to root widget is defined
        self.root = None
        
        # can show if not connected
        self.hostinfo = "[not connected: listening on port {}]".format(cfg.ws_port)

        # reset inactivity timer
        self.set_last_activity()
        
        # get content and test
        cfg.content = cfg.load_content(cfg.content_file)
        log.msg("Using content definition file '{}'.".format(cfg.content_file))
        if not cfg.test_content(cfg.content):
            raise "YAML configuration invalid"
        
        # dynamically load kv file 
        # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.load_kv says that for default file it is run before App.run()
        #
        # not used yet:
        log.msg("Now loading kv file '{}' ...".format(cfg.content["kv_file"]))
        Builder.load_file(cfg.content["kv_file"])
        
        # app title
        self.title = b"Touch Controller"
        
        # network setup with twisted
        factory = WebSocketServerFactory(url=cfg.factory_url)  # could leave this empy to avoid inconsistent ports???
        factory.protocol = WSProtocol
        reactor.listenTCP(cfg.ws_port, factory)
        
        self.root = TCRoot()
        self.root.reset_gui()  # set defaults
        
        # auto-cycle mode on start-up
        self.start_auto_cycle_mode()
        
        return self.root
        
        
    ### inactivity stuff
    #
    
    def start_auto_cycle_mode(self):
        
        if cfg.verbose > 1:
            log.msg("Starting auto-cycle mode. Stopping inactivity checker.")
        
        # no checks for inactivity needed
        if self.inactivity_check_event:
            self.inactivity_check_event.cancel()
            self.inactivity_check_event = None
        
        # start auto cycling
        if self.auto_cycle_event is None:
            self.auto_cycle_event = Clock.schedule_interval(self.root.cycle_forward, cfg.auto_cycle_interval)
    
    
    def stop_auto_cycle_mode(self):
        
        if cfg.verbose > 1:
            log.msg("Leaving auto-cycle mode. Starting inactivity checker.")
        
        # stop auto-cycling
        if self.auto_cycle_event:
            self.auto_cycle_event.cancel()
            self.auto_cycle_event = None
        
        # start regular checks for inactivity
        if self.inactivity_check_event is None:
            self.inactivity_check_event = Clock.schedule_interval(self.inactivity_checker, cfg.inactivity_check_interval)
    
    
    def set_last_activity(self):
        """Reset by inactivity timer"""
        
        self._last_activity = time.time()
        if self.auto_cycle_event is not None:
            self.stop_auto_cycle_mode()
        if cfg.verbose > 1:
            log.msg("Inactivity timer reset.")
    

    def inactivity_checker(self, dt):
        """Check for inactivity when not in auto-cycle mode."""
        if cfg.verbose > 2:
            log.msg("Checking for inactivity... {}".format(time.time()-self._last_activity))
        if time.time() - self._last_activity > cfg.inactivity_threshold_time:
            self.start_auto_cycle_mode()
            if cfg.verbose > 1:
                log.msg("Inactivity recognized.")
            #self.root.cycle_forward()
            #self.set_last_activity()
        
        
    def set_conn_status(self, text):
        """Sets the connection status, that will be shown by a widget."""
        self.root.ids["connstatus_id"].text = text
    
    
    ### simple callbacks triggered by widgets:
    #
    def action_state(self, *args):
        """Internal state (stored within the widgets) changed and requires action: network comms."""
        
        #print("action_state: ", args)
        self.send_state()
    
    
    def action_reset(self):
        if cfg.verbose > 1:
            log.msg("[+] Selected RESET")
        self.root.reset_gui()
        
        # After a reset, connection is lost.
        # New connection will always get full state on init, so no need to send here.
        self.send_reset()
    
    def action_language(self):
        if cfg.verbose > 1: 
            log.msg("[+] Language change")
        
        # select next language
        idx = (cfg.languages_available.index(cfg.lang) + 1) % len(cfg.languages_available)
        cfg.lang = cfg.languages_available[idx]
        if cfg.verbose > 1:
            log.msg("[+] New language: {}".format(cfg.lang))
        
        self.root.update_all()
        
        # all components may be affected
        #self.send_all()
        self.action_state()  # when and where??? TODO
        
        
    ### network (websocket) communications
    #
    def send_state(self):
        """Compile the current "state" and call send. 
        State is stored (and hence read) from the respective widgets."""
        
        state = {}
        
        # need init'ed widgets
        if self.root is not None:
            si = self.root.ids["selector_id"].selection_index
            switch_list = self.root.ids["switchpanel_id"].switch_list

            # selection
            state["texture"] = cfg.content["selection_menu"][si]["texture"]
            
            # switches
            for switch in switch_list:
                # get switch "identity"
                key = switch.ud["key"]
                if switch.state == "normal":
                    state[key] = cfg.content["selection_menu"][si][key]["off_texture"][cfg.lang]
                else:
                    state[key] = cfg.content["selection_menu"][si][key]["on_texture"][cfg.lang]
            
            self.send(state)
    
    
    def send_reset(self):
        self.send({"texture": "Reset"})
    
    
    def send(self, obj):
        """Encapsulation as json and sending."""
        
        msg = json.dumps(obj, ensure_ascii=False)
        
        if cfg.verbose > 0:
            log.msg("Sending message: {}.".format(msg))

        protocol = cfg.conn
        if protocol:
            protocol.sendMessage(msg.encode("utf8"))
        else: 
            if cfg.verbose > 0:
                log.msg("... Not connected. No message sent.")


###############################################################################
if __name__ == '__main__':
    try:
        TCApp().run()
    except KeyboardInterrupt:
        pass
    except:
        raise
    finally:
        # close server?
        pass
    

